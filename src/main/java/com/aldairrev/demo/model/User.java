package com.aldairrev.demo.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class User {
    private Integer id;
    private String username;
    private String password;
    private String token;

    public User() {

    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }
}
