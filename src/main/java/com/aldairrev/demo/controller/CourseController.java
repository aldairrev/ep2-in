package com.aldairrev.demo.controller;

import java.util.List;

import com.aldairrev.demo.model.Course;
import com.aldairrev.demo.service.ICourseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/courses")
public class CourseController {
    
    @Autowired
    private ICourseService courseService;

    @GetMapping("/")
    public List<Course> index() {
        return courseService.all();
    }

    @GetMapping("/{id}")
    public Course show(@PathVariable Long id) {
        return courseService.find(id);
    }

    @PostMapping("/")
    public Course create(@RequestBody Course course) {
        return courseService.save(course);
    }

    @PutMapping("/")
    public Course update(@RequestBody Course course) {
        return courseService.save(course);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id) {
        courseService.delete(id);
        return "Course Id: " + id + " deleted!";
    }
}
