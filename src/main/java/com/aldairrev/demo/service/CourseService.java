package com.aldairrev.demo.service;

import java.util.List;

import com.aldairrev.demo.model.Course;
import com.aldairrev.demo.repository.CourseRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService implements ICourseService {
    @Autowired
    private CourseRepository repository;

    @Override
    public List<Course> all() {
        return (List<Course>) repository.findAll();
    }

    @Override
    public Course find(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public Course save(Course course) {
        System.out.println(course.toString());
        return repository.save(course);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
