package com.aldairrev.demo.service;

import java.util.List;

import com.aldairrev.demo.model.Course;

import org.springframework.stereotype.Service;

@Service
public interface ICourseService {
    List<Course> all();
    Course find(Long id);
    Course save(Course course);
    void delete(Long id);
}
